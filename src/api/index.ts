import axios, { AxiosInstance, AxiosPromise } from 'axios';
import querystring from 'querystring';

var sharedService: API = null;

class API {
	public api: AxiosInstance;

	static get shared(): API {
		return sharedService;
	}

	constructor() {
		this.api = axios.create({
			baseURL: API_URL,
			timeout: 60000
		});
		if (!sharedService) sharedService = this;
	}

	get(url: string, query: any) {
		var queryString = querystring.stringify(query);
		url += `?${queryString}`;
		return this.api.get(url);
	}

	post(url: string, payload: any) {
		return this.api.post(url, payload);
	}

	patch(url: string, payload: any) {
		return this.api.patch(url, payload);
	}

	put(url: string, payload: any) {
		return this.api.put(url, payload);
	}

	delete(url: string, query: any) {
		var queryString = querystring.stringify(query);
		url += `?${queryString}`;
		return this.api.delete(url);
	}
}
new API();

export default API;
