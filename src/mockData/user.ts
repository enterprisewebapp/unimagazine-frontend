import User, { UserRoles } from "../data/User";

const user:User = {
    userId: 1,
    fullname: "Hoan Than",
    email: "hoanthan317@gmail.com",
    address: "Ho Chi Minh",
    facultyId: 1,
    status: 1,
    role: UserRoles.ADMIN,
    createdDate: new Date(2019,1,1),
    updatedDate: new Date()
}
export default user