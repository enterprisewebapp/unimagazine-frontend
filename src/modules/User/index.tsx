import UserModule from './UserModule';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import SessionReducerProps from '../../data/Session';
import User, { UserReducerProps } from '../../data/User';
import UserActions from '../../data/User/actions';

export interface UserModuleProps extends RouteComponentProps<{ parentPath: string }> {
	session: SessionReducerProps;
	users: UserReducerProps;
	loadUsers: (pageIndex: number, pageSize: number) => void;
	updateFaculty: (userId: number, faculty: string) => void;
	updateRole: (userId: number, role: string) => void;
	getByRole: (role: string, pageIndex: number, pageSize: number) => void;
	updateUser: (user: User) => void;
}

export interface UserSingleProps extends UserModuleProps {
	user: User;
	onUpdate: () => void;
}

const mapStateToProps = (state) => {
	return state;
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadUsers: (pageIndex: number, pageSize: number) => {
			dispatch(UserActions.loadUsers(pageIndex, pageSize));
		},
		updateFaculty: (userId: number, faculty: string) => {
			dispatch(UserActions.setFaculty(userId, faculty));
		},
		updateRole: (userId: number, role: string) => {
			dispatch(UserActions.setRole(userId, role));
		},
		getByRole: (role: string, pageIndex: number, pageSize: number) => {
			dispatch(UserActions.getByRole(role, pageIndex, pageSize));
		},
		updateUser: (user: User) => {
			dispatch(UserActions.update(user));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(UserModule);
