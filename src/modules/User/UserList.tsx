import React from 'react';
import { UserModuleProps } from '.';
import UserSingleRow from './components/UserSingleRow';
import User, { UserRoles } from '../../data/User';
import uuid from 'uuid/v4';
import ErrorMessage from '../../layouts/portal/components/ErrorMessage';
import SpinnerLayer from '../../layouts/portal/components/SpinnerLayer';

class State {
	constructor(
		public users: User[] = [],
		public pagination: {
			pageIndex: number;
			pageSize: number;
			total: number;
		} = {
			pageIndex: 0,
			pageSize: 1000,
			total: 0
		},
		public isLoading: boolean = false,
		public isUpdating: boolean = false,
		public errorMessage: string = ''
	) {}
}

export default class ArticleList extends React.Component<UserModuleProps, State> {
	constructor(props: UserModuleProps) {
		super(props);
		this.state = new State([], {
			pageSize: props.users.pagination.itemsPerPage,
			pageIndex: props.users.pagination.currentPage,
			total: props.users.pagination.totalCount
		});
	}

	componentDidMount() {
		this.loadUsers();
	}

	componentWillReceiveProps(nextProps: UserModuleProps) {
		if (this.state.isLoading && !nextProps.users.isLoading) {
			this.setState({
				isLoading: false,
				users: nextProps.users.data.filter((user) => user.role != UserRoles.ADMIN),
				pagination: {
					pageIndex: nextProps.users.pagination.currentPage,
					pageSize: nextProps.users.pagination.itemsPerPage,
					total: nextProps.users.pagination.totalCount
				},
				errorMessage: nextProps.users.error
					? nextProps.users.error.message || 'Something went wrong. Please try again later!'
					: ''
			});
		}
		if (this.state.isUpdating && !nextProps.users.isUpdating) {
			this.setState({
				isUpdating: false
			});
			this.loadUsers();
		}
	}

	loadUsers() {
		this.setState({ isLoading: true });
		this.props.loadUsers(this.state.pagination.pageIndex, this.state.pagination.pageSize);
	}

	render() {
		return (
			<div>
				{this.state.isLoading ? (
					<SpinnerLayer loading={this.state.isLoading || this.state.isUpdating} />
				) : (
					<div>
						<div className="justify-content-between">
							<h2>Users</h2>
						</div>
						{this.state.errorMessage ? <ErrorMessage messages={[ this.state.errorMessage ]} /> : null}
						<div className="list-container">
							{this.state.users.map((user) => {
								return (
									<UserSingleRow
										key={uuid()}
										{...this.props}
										user={user}
										onUpdate={() => {
											this.setState({
												isUpdating: true
											});
										}}
									/>
								);
							})}
						</div>
					</div>
				)}
			</div>
		);
	}
}
