import React from 'react';
import { Switch, Route } from 'react-router';
import UserList from './UserList';
import { UserModuleProps } from '.';

export default class UserModule extends React.Component<UserModuleProps> {
	render() {
		return (
			<Switch>
				<Route
					path={`:parentPath(.*)/users`}
					render={(routerProps) => {
						return <UserList {...this.props} {...routerProps} />;
					}}
				/>
				<Route
					path={`:parentPath(.*)`}
					render={(routerProps) => {
						return <UserList {...this.props} {...routerProps} />;
					}}
				/>
			</Switch>
		);
	}
}
