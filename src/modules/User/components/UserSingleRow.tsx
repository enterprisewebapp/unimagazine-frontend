import React from 'react';
import { UserSingleProps } from '..';
import inflection from 'inflection';
import uuid from 'uuid/v4';
import User, { UserRoles, Faculties } from '../../../data/User';
import ErrorMessage from '../../../layouts/portal/components/ErrorMessage';
import { Modal, Button } from 'react-bootstrap';

class State {
	constructor(
		public user: User,
		public errorMessages: string[] = [],
		public showEditModal: boolean = false,
		public isSending: boolean = false
	) {}
}

export default class UserSingleRow extends React.Component<UserSingleProps, State> {
	constructor(props: UserSingleProps) {
		super(props);
		this.state = new State(props.user);
	}

	handleCloseEditModal() {
		this.setState({
			showEditModal: false,
			user: this.props.user
		});
	}

	handleUpdateUser(event) {
		event.preventDefault();
		var validationErrors: string[] = [];
		var user = Object.assign({}, this.state.user);
		if (!user.fullname) validationErrors.push(`User name is required`);
		if (!user.email) validationErrors.push(`User email is required`);
		this.setState({
			errorMessages: validationErrors
		});
		if (validationErrors.length) return;

		if (this.state.user.faculty != this.props.user.faculty) {
			this.props.onUpdate();
			this.props.updateFaculty(this.props.user.userId, this.state.user.faculty);
		}
		if (this.state.user.role != this.props.user.role) {
			this.props.onUpdate();
			this.props.updateRole(this.props.user.userId, this.state.user.role);
		}
		if (this.state.user.address != this.props.user.address) {
			this.props.onUpdate();
			this.props.updateUser(
				Object.assign({}, this.props.user, {
					address: this.state.user.address
				})
			);
		}
	}

	render() {
		const roles = [
			{ value: UserRoles.ADMIN, label: 'Admin' },
			{ value: UserRoles.MARKETING_MANAGER, label: 'Marketing Manager' },
			{ value: UserRoles.MARKETING_COORDINATOR, label: 'Marketing Coordinator' },
			{ value: UserRoles.STUDENT, label: 'Student' },
			{ value: UserRoles.GUEST, label: 'Guest' }
		];
		return (
			<div>
				<div className="table-row-single">
					{this.state.showEditModal ? (
						<div className="row col">
							<form className="col-12">
								<div className="form-group">
									<label>Full name</label>
									<input
										type="text"
										disabled
										value={this.state.user.fullname}
										className="form-control"
										maxLength={255}
										onChange={(event) => {
											this.setState({
												user: Object.assign({}, this.state.user, {
													fullname: event.target.value
												})
											});
										}}
									/>
								</div>
								<div className="form-group">
									<label>Email</label>
									<input
										type="email"
										disabled
										value={this.state.user.email}
										className="form-control"
										maxLength={255}
										onChange={(event) => {
											this.setState({
												user: Object.assign({}, this.state.user, {
													email: event.target.value
												})
											});
										}}
									/>
								</div>
								<div className="form-group">
									<label>Address</label>
									<input
										type="text"
										disabled={this.props.session.userProfile.userId != this.props.user.userId}
										value={this.state.user.address}
										className="form-control"
										maxLength={255}
										onChange={(event) => {
											this.setState({
												user: Object.assign({}, this.state.user, {
													address: event.target.value
												})
											});
										}}
									/>
								</div>
								<div className="form-group">
									<label>Role</label>
									<select
										className="form-control"
										value={this.state.user.role}
										disabled={this.props.session.userProfile.role != UserRoles.ADMIN}
										onChange={(event) => {
											this.setState({
												user: Object.assign({}, this.state.user, {
													role: event.target.value
												})
											});
										}}
									>
										{roles.map((role) => {
											return (
												<option
													disabled={
														role.value == 'admin' &&
														this.props.session.userProfile.role != UserRoles.ADMIN
													}
													key={uuid()}
													value={role.value}
												>
													{role.label}
												</option>
											);
										})}
									</select>
								</div>
								<div className="form-group">
									<label>Faculty</label>
									<select
										className="form-control"
										value={this.state.user.faculty}
										disabled={this.props.session.userProfile.role != UserRoles.ADMIN}
										onChange={(event) => {
											this.setState({
												user: Object.assign({}, this.state.user, {
													faculty: event.target.value
												})
											});
										}}
									>
										{Object.keys(Faculties).map((key) => {
											return (
												<option key={uuid()} value={key}>
													{Faculties[key]}
												</option>
											);
										})}
									</select>
								</div>
								{this.state.errorMessages && this.state.errorMessages.length ? (
									<ErrorMessage messages={this.state.errorMessages} />
								) : null}
							</form>
							<div className="col">
								<button className="btn btn-secondary" onClick={this.handleCloseEditModal.bind(this)}>
									Close
								</button>
								<button className="btn btn-primary ml-2" onClick={this.handleUpdateUser.bind(this)}>
									Save
								</button>
							</div>
						</div>
					) : (
						<div className="col-12 row">
							<div className="col-9">
								<div className="col-12">
									<b className="text-primary">{this.props.user.fullname}</b>
								</div>
								<div className="col-12">
									<small className="pr-3">
										<b>Email</b>: {this.props.user.email || '--'}
									</small>
									<small className="pr-3">
										<b>Address</b>: {this.props.user.address || '--'}
									</small>
									<small className="pr-3">
										<b>Role</b>:{' '}
										{inflection.titleize(inflection.humanize(this.props.user.role || '')) || '--'}
									</small>
									<small className="pr-3">
										<b>Faculty</b>: {Faculties[this.props.user.faculty] || '--'}
									</small>
								</div>
							</div>
							<div className="col-3 pt-1">
								<div
									className={`actions float-right ${this.props.session.userProfile.role !=
									UserRoles.ADMIN
										? 'd-none'
										: ''}`}
								>
									{this.state.showEditModal ? null : (
										<button
											className="btn btn-outline-primary mr-2"
											onClick={(event) => {
												event.preventDefault();
												this.setState({
													showEditModal: true
												});
											}}
										>
											Edit
										</button>
									)}
								</div>
							</div>
						</div>
					)}
				</div>
			</div>
		);
	}
}
