import React from 'react';
import { ArticleSingleProps } from '.';
import ArticleEditForm from './components/EditArticleForm';

export default class EditArticle extends React.Component<ArticleSingleProps> {
	private form: ArticleEditForm;

	componentWillReceiveProps(props: ArticleSingleProps) {
		if (this.props.articles.isUpdating && !props.articles.isUpdating) {
			const queryTags = new URLSearchParams(this.props.location.search);
			this.props.loadArticles(0, 1000, queryTags.get('faculty'));
		}
	}

	render() {
		return (
			<div>
				<div className="d-flex justify-content-between">
					<h2>Edit Article</h2>
					<div className="justify-content-end">
						<button
							className="btn btn-primary"
							onClick={() => {
								this.form.handleSubmit();
							}}
						>
							Submit
						</button>
					</div>
				</div>
				<ArticleEditForm onRef={(ref) => (this.form = ref as ArticleEditForm)} {...this.props} />
				<div>
					<button className="btn btn-link pl-0 mt-2" onClick={() => this.props.history.goBack()}>
						Back
					</button>
				</div>
			</div>
		);
	}
}
