import React from 'react';
import { ArticleModuleProps } from '.';
import uuid from 'uuid/v4';
import ArticleSingleRow from './components/ArticleSingleRow';
import SelectSearch from 'react-select-search';
import { Faculties, UserRoles } from '../../data/User';
import Article from '../../data/Article';
import SpinnerLayer from '../../layouts/portal/components/SpinnerLayer';
import Year from '../../data/Year';
import moment = require('moment');

class State {
	constructor(
		public articles: Article[] = [],
		public isLoading: boolean = false,
		public pageIndex: number = 0,
		public pageSize: number = 1000,
		public errorMessage: string = '',
		public faculty: string = ''
	) {}
}

export default class ArticleList extends React.Component<ArticleModuleProps, State> {
	constructor(props: ArticleModuleProps) {
		super(props);
		this.state = new State();
	}

	componentDidMount() {
		if (!this.state.isLoading) this.loadArticles();
	}

	componentWillReceiveProps(props: ArticleModuleProps) {
		if (this.state.isLoading && !props.articles.isLoading) {
			this.setState({
				isLoading: false,
				articles: props.articles.data
			});
			if (props.articles.error) {
				this.setState({
					errorMessage: props.articles.error.message || 'Something went wrong. Please try again later!'
				});
			}
		}
	}

	loadArticles(faculty?: string) {
		if (!faculty) {
			if (
				this.props.session.userProfile.role != UserRoles.ADMIN &&
				this.props.session.userProfile.role != UserRoles.MARKETING_MANAGER
			) {
				faculty = this.props.session.userProfile.faculty;
			}
		}
		this.setState(
			{
				isLoading: true
			},
			() => {
				this.props.loadArticles(this.state.pageIndex, this.state.pageSize, faculty);
			}
		);
	}

	render() {
		const currentYear: Year = this.props.years.data.find((year) => year.currentFlag == 1);
		return (
			<div>
				{this.state.isLoading ? <SpinnerLayer loading={this.state.isLoading} /> : null}
				<div className="d-flex justify-content-between">
					<h2>Articles</h2>
					<div className="d-flex justify-content-end">
						<div className="pt-1">
							<button className="btn btn-link" onClick={() => this.loadArticles()}>
								<i className="fa fa-refresh" aria-hidden="true" /> Refresh
							</button>
						</div>
						<SelectSearch
							options={Object.keys(Faculties).map((key) => {
								return {
									value: key,
									name: Faculties[key]
								};
							})}
							onChange={(option) => {
								this.loadArticles(option.value);
								this.setState({
									faculty: option.value
								});
							}}
							placeholder="Select Faculty"
							value={this.props.session.userProfile.faculty || undefined}
							className={`${this.props.session.userProfile.role != UserRoles.ADMIN &&
							this.props.session.userProfile.role != UserRoles.MARKETING_MANAGER
								? 'd-none'
								: 'select-search-box'}`}
						/>
						<div className="pt-1">
							<button
								disabled={currentYear && moment(currentYear.closureDate1).isBefore(moment())}
								className={`btn btn-primary justify-content-end ml-3 ${this.props.session.userProfile
									.role != UserRoles.ADMIN && this.props.session.userProfile.role != UserRoles.STUDENT
									? 'd-none'
									: ''}`}
								onClick={() =>
									this.props.history.push(`${this.props.match.params.parentPath || ''}/articles/new`)}
							>
								Upload
							</button>
						</div>
					</div>
				</div>
				<div className="list-container">
					{this.props.articles.data.map((article) => {
						return (
							<ArticleSingleRow
								key={uuid()}
								article={article}
								{...this.props}
								faculty={this.state.faculty}
							/>
						);
					})}
				</div>
			</div>
		);
	}
}
