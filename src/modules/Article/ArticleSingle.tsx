import React from 'react';
import { ArticleSingleProps } from '.';
import ArticleDetails from './components/ArticleDetails';
import { ArticleStatus } from '../../data/Article';
import { UserRoles } from '../../data/User';
import moment = require('moment');

export default class ArticleSingle extends React.Component<ArticleSingleProps> {
	constructor(props) {
		super(props);
	}

	componentWillReceiveProps(props: ArticleSingleProps) {
		if (this.props.articles.isUpdating && !props.articles.isUpdating) {
			const queryTags = new URLSearchParams(this.props.location.search);
			this.props.loadArticles(0, 1000, queryTags.get('faculty'));
		}
	}

	updateStatus(status: number) {
		this.props.updateStatus(status, this.props.article.threadId);
	}

	render() {
		const statusClass = {
			0: 'warning',
			1: 'danger',
			2: 'info',
			3: 'primary'
		};
		const yearConfig: any = this.props.years.data.find((year) => year.currentFlag == 1) || {};
		return (
			<div>
				<div className="d-flex justify-content-between">
					<h2>
						{this.props.article.threadTitle}{' '}
						<span className={`badge badge-${statusClass[this.props.article.status]}`}>
							<small>{ArticleStatus[this.props.article.status]}</small>
						</span>
					</h2>
					{this.props.session.userProfile.role == UserRoles.ADMIN ||
					this.props.session.userProfile.role == UserRoles.MARKETING_COORDINATOR ? (
						<div>
							{this.props.article.status == 0 ? (
								<button
									className="btn btn-outline-warning"
									disabled={this.props.articles.isUpdating}
									onClick={() => this.updateStatus(1)}
								>
									Reject
								</button>
							) : null}
							{this.props.article.status != 2 && this.props.article.status != 3 ? (
								<button className="btn btn-primary ml-2" onClick={() => this.updateStatus(2)}>
									Approve
								</button>
							) : null}
						</div>
					) : null}
					{this.props.article.status == 2 &&
					(this.props.session.userProfile.role == UserRoles.MARKETING_MANAGER ||
						this.props.session.userProfile.role == UserRoles.ADMIN) ? (
						<button className="btn btn-primary ml-2" onClick={() => this.updateStatus(3)}>
							Publish
						</button>
					) : null}
					{this.props.session.userProfile.role == UserRoles.STUDENT &&
					this.props.session.userProfile.userId == this.props.article.createdBy.userId ? (
						<div>
							<button
								disabled={moment(yearConfig.closureDate2).isBefore(moment())}
								className="btn btn-primary"
								onClick={() =>
									this.props.history.push(
										`${this.props.match.params.parentPath || ''}/articles/${this.props.article
											.threadId}/edit`
									)}
							>
								Edit
							</button>
						</div>
					) : null}
				</div>
				<ArticleDetails {...this.props} />
			</div>
		);
	}
}
