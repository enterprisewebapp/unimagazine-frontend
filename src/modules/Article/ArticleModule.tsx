import React from 'react';
import { ArticleModuleProps } from '.';
import { Switch, Route } from 'react-router';
import ArticleList from './ArticleList';
import '../../assets/css/article.css';
import NewArticle from './Create';
import ArticleSingle from './ArticleSingle';
import Edit from './Edit';

export default class ArticleModule extends React.Component<ArticleModuleProps> {
	componentDidMount() {
		this.props.loadYears(0, 1000);
	}

	render() {
		return (
			<Switch>
				<Route
					path={`:parentPath(.*)/articles/new`}
					render={(routerProps) => {
						return <NewArticle {...this.props} {...routerProps} />;
					}}
				/>
				<Route
					path={`:parentPath(.*)/articles/:article_id/edit`}
					render={(routerProps) => {
						var articleId = routerProps.match.params.article_id;
						if (!articleId) return <div />;
						var article = this.props.articles.data.find((article) => article.threadId == articleId);
						if (!article) return <div />;
						return <Edit article={article} {...this.props} {...routerProps} />;
					}}
				/>
				<Route
					path={`:parentPath(.*)/articles/:article_id`}
					render={(routerProps) => {
						var articleId = routerProps.match.params.article_id;
						if (!articleId) return <div />;
						var article = this.props.articles.data.find((article) => article.threadId == articleId);
						if (!article) return <div />;
						return <ArticleSingle article={article} {...this.props} {...routerProps} />;
					}}
				/>
				<Route
					path={`:parentPath(.*)/articles`}
					render={(routerProps) => {
						return <ArticleList {...this.props} {...routerProps} />;
					}}
				/>
				<Route
					path={`:parentPath(.*)`}
					render={(routerProps) => {
						return <ArticleList {...this.props} {...routerProps} />;
					}}
				/>
			</Switch>
		);
	}
}
