import React from 'react';
import { ArticleSingleProps } from '..';

class State {
	constructor(public comment: string = '', public isSending: boolean = false) {}
}

export default class CommentTool extends React.Component<ArticleSingleProps, State> {
	constructor(props) {
		super(props);
		this.state = new State();
	}

	handleSubmit() {
		if (!this.state.comment || this.props.articles.isCommentCreating) return;
		this.props.createComment(this.state.comment, this.props.article.threadId);
		this.setState({
			comment: ''
		});
	}

	render() {
		return (
			<form
				className="form-inline row ml-0 mr-1"
				onSubmit={(event) => {
					event.preventDefault();
					this.handleSubmit();
				}}
			>
				<div className="form-group d-flex justify-content-between" style={{ width: '100%' }}>
					<input
						required
						maxLength={255}
						className="form-control"
						style={{ width: '90%' }}
						value={this.state.comment}
						onChange={(event) => {
							this.setState({
								comment: event.target.value
							});
						}}
					/>
					<button type="submit" className="btn btn-primary justify-content-end">
						Comment
					</button>
				</div>
			</form>
		);
	}
}
