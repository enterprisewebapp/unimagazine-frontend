import React from 'react';
import { ArticleSingleProps } from '..';
import Article from '../../../data/Article';
import ThreadFile from '../../../data/ThreadFile';
import Axios from 'axios';
import ErrorMessage from '../../../layouts/portal/components/ErrorMessage';
import FileSaver from 'file-saver';
import JsZip from 'jszip';
import moment from 'moment';
import CommentTool from './CommentTool';
import uuid from 'uuid/v4';
import { UserRoles } from '../../../data/User';

class State {
	constructor(public isZippingFiles: boolean = false, public errorMessage: string = '') {}
}

export default class ArticleDetails extends React.Component<ArticleSingleProps, State> {
	constructor(props) {
		super(props);
		this.state = new State();
	}

	componentWillReceiveProps(props: ArticleSingleProps) {
		if (this.props.articles.isCommentCreating && !props.articles.isCommentCreating) {
			const queryTags = new URLSearchParams(this.props.location.search);
			this.props.loadArticles(0, 1000, queryTags.get('faculty'));
		}
	}

	zipAndDownloadImages(images: ThreadFile[]) {
		this.setState({
			isZippingFiles: true
		});
		var promises: Promise<void>[] = [];
		var blobs: {
			name: string;
			blob: Blob;
		}[] = [];
		images.forEach((image) => {
			promises.push(
				new Promise<void>((rs) => {
					Axios({
						url: `${FILE_ROOT}/${image.path}`,
						method: 'GET',
						responseType: 'blob'
					})
						.then((response) => {
							blobs.push({
								name: image.path.substr(image.path.lastIndexOf('/')),
								blob: response.data
							});
							rs();
						})
						.catch((error) => {
							this.setState({
								errorMessage:
									error.response.data.message ||
									'Cannot compress image files. Please try again later!',
								isZippingFiles: false
							});
						});
				})
			);
		});
		Promise.all(promises)
			.then(() => {
				var zip = new JsZip();
				var folder = zip.folder('images');
				blobs.forEach((blob) => {
					folder.file(blob.name, blob.blob);
				});
				zip
					.generateAsync({ type: 'blob' })
					.then((value) => {
						FileSaver.saveAs(value, `${this.props.article.threadTitle}-images.zip`);
					})
					.catch((error) => {
						this.setState({
							errorMessage:
								error.response.data.message || 'Cannot compress image files. Please try again later!',
							isZippingFiles: false
						});
					});
			})
			.catch((error) => {
				this.setState({
					errorMessage: error.response.data.message || 'Cannot compress image files. Please try again later!',
					isZippingFiles: false
				});
			});
	}

	render() {
		const article: Article = this.props.article;
		const wordDoc: ThreadFile = article.threadFiles.find((a) => a.fileType == 'word');
		const images: ThreadFile[] = article.threadFiles.filter((a) => a.fileType == 'img');
		return (
			<div className="mb-5">
				<ErrorMessage messages={[ this.state.errorMessage ]} />
				<div>
					<p>
						<small>Author: {article.createdBy.fullname}</small>
					</p>
					<p>
						<small>Submitted at: {moment(article.createdDate).format('YYYY-MM-DD')}</small>
					</p>
					<p>
						<small>Last updated at: {moment(article.updatedDate).format('YYYY-MM-DD')}</small>
					</p>
				</div>
				<div className="mt-2">
					<p>
						<b>Description</b>
					</p>
					<p>{article.description}</p>
				</div>
				<div className="mt-2">
					{wordDoc ? (
						<a download href={`${FILE_ROOT}/${wordDoc.path}`}>
							<i className="fa fa-cloud-download" />&nbsp; Document
						</a>
					) : null}
					{images && images.length ? (
						<a
							href="javascript:void(0)"
							className="ml-2"
							onClick={(event) => {
								event.preventDefault();
								if (!this.state.isZippingFiles) {
									this.zipAndDownloadImages(images);
								}
							}}
						>
							<i className="fa fa-cloud-download" />&nbsp; Images
						</a>
					) : null}
				</div>
				{this.props.session.userProfile.role != UserRoles.GUEST ? (
					<div className="mt-3">
						<b>Comments</b>
						<div className="mb-2">
							{this.props.article.threadComments.map((comment) => {
								return (
									<div key={uuid()} className="comment-box">
										<b>{comment.createdBy.fullname}</b>
										<div className="comment">
											<p>{comment.content}</p>
											<div>
												<small>
													Created at: {moment(comment.createdDate).format('YYYY-MM-DD')}
												</small>
											</div>
										</div>
									</div>
								);
							})}
						</div>
						{this.props.session.userProfile.role == UserRoles.ADMIN ||
						this.props.session.userProfile.role == UserRoles.MARKETING_COORDINATOR ||
						this.props.session.userProfile.userId == this.props.article.createdBy.userId ? (
							<CommentTool {...this.props} />
						) : null}
					</div>
				) : null}
				<div className="mt-2">
					<button
						onClick={() => {
							this.props.history.goBack();
						}}
						className="btn btn-link p-0"
					>
						Back
					</button>
				</div>
			</div>
		);
	}
}
