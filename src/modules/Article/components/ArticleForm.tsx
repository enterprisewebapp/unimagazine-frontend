import React from 'react';
import { ArticleFormProps } from '..';
import { ArticleFormData } from '../../../data/Article';
import ErrorMessage from '../../../layouts/portal/components/ErrorMessage';
import SpinnerLayer from '../../../layouts/portal/components/SpinnerLayer';

class State {
	constructor(
		public article: ArticleFormData = {
			threadTitle: '',
			description: '',
			images: [],
			wordFile: null
		},
		public errorMessages: string[] = [],
		public isCreating: boolean = false,
		public termAcceptted: boolean = false
	) {}
}

export default class ArticleForm extends React.Component<ArticleFormProps, State> {
	constructor(props: ArticleFormProps) {
		super(props);
		this.state = new State();
		if (this.props.onRef) this.props.onRef(this);
	}

	componentWillReceiveProps(props: ArticleFormProps) {
		if (this.state.isCreating && !props.articles.isCreating) {
			this.setState({
				isCreating: false
			});
			if (props.articles.error) {
				this.setState({
					errorMessages: [ props.articles.error.message ]
				});
			} else {
				this.props.history.push(`${this.props.match.params.parentPath || ''}/articles`);
			}
		}
	}

	handleSubmit() {
		var validationErrors: string[] = [];
		if (!this.state.article.threadTitle) validationErrors.push('Article title is required');
		if (!this.state.article.description) validationErrors.push('Article description is required');
		if (!this.state.article.wordFile) validationErrors.push('Article Word document is required');
		if (!this.state.termAcceptted) validationErrors.push("You must agree to the year's Terms & Conditions");

		this.setState({
			errorMessages: validationErrors
		});
		if (validationErrors.length) return;
		this.setState({
			isCreating: true
		});

		this.props.createArticle(this.state.article);
	}

	render() {
		const yearConfig: any = this.props.years.data.find((year) => year.currentFlag == 1) || {};
		return (
			<div>
				<SpinnerLayer loading={this.state.isCreating} />
				<ErrorMessage messages={this.state.errorMessages} />
				<div className="card mt-2">
					<div className="card-header">Title</div>
					<div className="card-body">
						<input
							className="form-control"
							value={this.state.article.threadTitle}
							onChange={(event) =>
								this.setState({
									article: Object.assign({}, this.state.article, {
										threadTitle: event.target.value
									})
								})}
						/>
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Description</div>
					<div className="card-body">
						<textarea
							className="form-control"
							value={this.state.article.description}
							onChange={(event) =>
								this.setState({
									article: Object.assign({}, this.state.article, {
										description: event.target.value
									})
								})}
						/>
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Attachment</div>
					<div className="card-body">
						<label className="btn btn-outline-primary">
							{this.state.article.wordFile ? 'Change Document' : 'Document'}
							<input
								type="file"
								accept=".doc, .docx"
								className="d-none"
								onChange={(event) => {
									var file = event.target.files[0];
									if (file) {
										this.setState({
											article: Object.assign({}, this.state.article, {
												wordFile: file
											})
										});
									}
								}}
							/>
						</label>
						<label className="btn btn-outline-primary ml-2">
							{this.state.article.images && this.state.article.images.length ? 'Change Images' : 'Images'}
							<input
								type="file"
								multiple
								accept="image/*"
								className="d-none"
								onChange={(event) => {
									var files = event.target.files;
									this.setState({
										article: Object.assign({}, this.state.article, {
											images: files || []
										})
									});
								}}
							/>
						</label>
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Terms & Conditions</div>
					<div className="card-body">{yearConfig.termContent}</div>
				</div>
				<div className="mt-2">
					<label>
						<input
							type="checkbox"
							checked={this.state.termAcceptted}
							onChange={(event) => {
								this.setState({
									termAcceptted: event.target.checked
								});
							}}
						/>{' '}
						I have read and agree to the Terms & Conditions
					</label>
				</div>
			</div>
		);
	}
}
