import React from 'react';
import { ArticleSingleProps } from '..';
import { ArticleStatus } from '../../../data/Article';
import { Faculties } from '../../../data/User';

export default class ArticleSingleRow extends React.Component<ArticleSingleProps> {
	render() {
		const statusClass = {
			0: 'warning',
			1: 'danger',
			2: 'info',
			3: 'primary'
		};
		return (
			<div className="col-12 table-row-single">
				<div className="col-12">
					<button
						className="btn btn-link p-0 mr-2"
						onClick={() =>
							this.props.history.push(
								`${this.props.match.params.parentPath || ''}/articles/${this.props.article
									.threadId}${this.props.faculty ? `?faculty=${this.props.faculty}` : ''}`
							)}
					>
						{this.props.article.threadTitle}
					</button>
					<span className={`badge badge-${statusClass[this.props.article.status]}`}>
						{ArticleStatus[this.props.article.status]}
					</span>
				</div>
				<div className="col-12">
					<small>{this.props.article.threadComments.length} comments</small>
					<small className="ml-2">Faculty: {Faculties[this.props.article.faculty] || 'All'}</small>
					<small className="float-right">
						Posted by: {this.props.article.createdBy.fullname || 'unknown'}
					</small>
				</div>
			</div>
		);
	}
}
