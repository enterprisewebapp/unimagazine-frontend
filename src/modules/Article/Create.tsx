import React from 'react';
import ArticleForm from './components/ArticleForm';
import { ArticleModuleProps } from '.';

export default class NewArticle extends React.Component<ArticleModuleProps> {
	private form: ArticleForm;
	private events: {
		[key: string]: () => any;
	} = {};
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<div className="d-flex justify-content-between">
					<h2>New Article</h2>
					<div className="justify-content-end">
						<button
							className="btn btn-primary"
							onClick={() => {
								this.form.handleSubmit();
							}}
						>
							Submit
						</button>
					</div>
				</div>
				<ArticleForm onRef={(ref) => (this.form = ref as ArticleForm)} {...this.props} />
				<div>
					<button className="btn btn-link pl-0 mt-2" onClick={() => this.props.history.goBack()}>
						Back
					</button>
				</div>
			</div>
		);
	}
}
