import { connect } from 'react-redux';
import ArticleModule from './ArticleModule';
import { AppProps } from '../../App';
import Article, { ArticleReducerProps, ArticleFormData } from '../../data/Article';
import { Component } from 'react';
import ArticleActions from '../../data/Article/actions';
import { YearReducerProps } from '../../data/Year';
import YearActions from '../../data/Year/actions';

export interface ArticleModuleProps extends AppProps {
	articles: ArticleReducerProps;
	years: YearReducerProps;
	createArticle: (data: ArticleFormData) => void;
	loadArticles: (pageIndex: number, pageSize: number, faculty?: string) => void;
	loadYears: (pageIndex: number, pageSize: number) => void;
	createComment: (comment: string, threadId: number) => void;
	updateStatus: (status: number, threadId: number) => void;
	update: (data: any) => void;
}

export interface ArticleSingleProps extends ArticleModuleProps {
	article: Article;
	onRefresh?: () => void;
	faculty?: string;
}

export interface ArticleFormProps extends ArticleModuleProps {
	onRef?: (component: Component) => void;
	article?: Article;
}

const mapStateToProps = (state: any) => {
	return state;
};

const mapDispatchToProps = (dispatch: any) => {
	return {
		createArticle: (data: ArticleFormData) => {
			dispatch(ArticleActions.create(data));
		},
		loadArticles: (pageIndex: number, pageSize: number, faculty?: string) => {
			dispatch(ArticleActions.load(pageIndex, pageSize, faculty));
		},
		loadYears: (pageIndex: number, pageSize: number) => {
			dispatch(YearActions.loadYears(pageIndex, pageSize));
		},
		createComment: (comment: string, threadId: number) => {
			dispatch(ArticleActions.addComment(comment, threadId));
		},
		updateStatus: (status: number, threadId: number) => {
			dispatch(ArticleActions.updateStatus(status, threadId));
		},
		update: (data: any) => {
			dispatch(ArticleActions.update(data));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleModule);
