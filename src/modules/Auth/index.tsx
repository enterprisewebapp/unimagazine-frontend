import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import AuthModule from './AuthModule';
import SessionReducerProps from '../../data/Session';
import SessionActions from '../../data/Session/actions';
import User from '../../data/User';

export interface AuthProps extends RouteComponentProps<{ parentPath: string }> {
	session: SessionReducerProps;
	login: (email: string, password: string) => void;
	register: (user: Partial<User>) => void;
}

const mapStateToProps = (state: any) => {
	return {
		session: state.session
	};
};

const mapDispatchToProps = (dispatch: any) => {
	return {
		login: (email: string, password: string) => {
			dispatch(SessionActions.login(email, password));
		},
		register: (user: Partial<User>) => {
			dispatch(SessionActions.register(user));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthModule);
