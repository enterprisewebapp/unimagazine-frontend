import React from 'react';
import { Switch, Route } from 'react-router';
import LoginForm from './components/LoginForm';
import { AuthProps } from '.';
import RegisterForm from './components/RegisterForm';

export default class AuthModule extends React.Component<AuthProps> {
	componentDidMount() {
		if (!this.props.session.token) this.props.history.push(`/login`);
	}

	render() {
		return (
			<div className="container text-center auth-form">
				<Switch>
					<Route
						path={':parentPath(.*)/register'}
						render={(routerProps) => {
							return <RegisterForm {...this.props} {...routerProps} />;
						}}
					/>
					<Route
						path={':parentPath(.*)/login'}
						render={(routerProps) => {
							return <LoginForm {...this.props} {...routerProps} />;
						}}
					/>
				</Switch>
			</div>
		);
	}
}
