import React from 'react';
import { AuthProps } from '..';

import SpinnerLayer from '../../../layouts/portal/components/SpinnerLayer';
import { Link } from 'react-router-dom';

const logo = require('../../../assets/images/logo.png');

class State {
	constructor(
		public isSending: boolean = false,
		public errorMessage: string = '',
		public email: string = '',
		public password: string = '',
		public fullname: string = '',
		public address: string = '',
		public passwordRepeat: string = '',
		public isPasswordMatched: boolean = true
	) {}
}

export default class RegisterForm extends React.Component<AuthProps, State> {
	constructor(props: AuthProps) {
		super(props);
		this.state = new State();
	}

	componentWillReceiveProps(nextProps: AuthProps) {
		if (this.state.isSending && !nextProps.session.isRegistering) {
			this.setState({ isSending: false });
			if (nextProps.session.error) {
				this.setState({
					errorMessage: nextProps.session.error.message || `Something went wrong. Please try again later!`
				});
			} else this.props.history.push('/login');
		}
	}

	handleSubmit() {
		var user = Object.assign({}, this.state);
		var validationError = '';
		if (!this.state.isPasswordMatched) return;
		this.setState({
			isSending: true
		});
		this.props.register(user);
	}

	render() {
		return (
			<div>
				<SpinnerLayer loading={this.state.isSending} />
				<div className="m-3">
					<img src={logo} />
				</div>
				<form
					className="form"
					onSubmit={(event) => {
						event.preventDefault();
						this.handleSubmit();
					}}
				>
					<div className="form-group">
						<input
							type="email"
							placeholder="Email (required)"
							required
							className="form-control"
							onChange={(event) => {
								this.setState({
									email: event.target.value
								});
							}}
						/>
					</div>
					<div className="form-group">
						<input
							type="password"
							placeholder="Password (required)"
							required
							className="form-control"
							onChange={(event) => {
								this.setState({
									password: event.target.value
								});
							}}
						/>
					</div>
					<div className="form-group">
						<input
							type="password"
							placeholder="Repeat password (required)"
							required
							className="form-control"
							onChange={(event) => {
								this.setState({
									passwordRepeat: event.target.value,
									errorMessage:
										event.target.value != this.state.password ? 'Password not match!' : '',
									isPasswordMatched: event.target.value == this.state.password
								});
							}}
						/>
					</div>
					<div className="form-group">
						<input
							type="text"
							placeholder="Full name (required)"
							required
							className="form-control"
							onChange={(event) => {
								this.setState({
									fullname: event.target.value
								});
							}}
						/>
					</div>
					<div className="form-group">
						<input
							type="text"
							placeholder="Address (optional)"
							className="form-control"
							onChange={(event) => {
								this.setState({
									address: event.target.value
								});
							}}
						/>
					</div>
					<div className="text-left">
						{this.state.errorMessage ? <p className="text-danger pb-1">{this.state.errorMessage}</p> : ``}
					</div>

					<div className="form-group">
						<button type="submit" className="btn btn-primary form-control" disabled={this.state.isSending}>
							Submit
						</button>
						<label>
							Already have an account? <Link to="/login">Login</Link> now!
						</label>
					</div>
				</form>
			</div>
		);
	}
}
