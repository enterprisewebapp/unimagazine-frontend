import React from 'react';
import { AuthProps } from '..';
import SpinnerLayer from '../../../layouts/portal/components/SpinnerLayer';

const logo = require('../../../assets/images/logo.png');

class State {
	constructor(
		public email: string = '',
		public password: string = '',
		public isSending: boolean = false,
		public errorMessage: string = ''
	) {}
}

export default class LoginForm extends React.Component<AuthProps, State> {
	constructor(props: AuthProps) {
		super(props);
		this.state = new State();
	}
	componentWillReceiveProps(nextProps: AuthProps) {
		if (this.state.isSending && !nextProps.session.isLoggingIn) {
			this.setState({
				isSending: false
			});
			if (!nextProps.session.token) {
				if (nextProps.session.error) {
					this.setState({ errorMessage: `Something went wrong. Please try again later!` });
				} else this.setState({ errorMessage: `Invalid email or password` });
			}
		}
	}
	handleSubmit() {
		this.setState({
			isSending: true
		});

		this.props.login(this.state.email, this.state.password);
	}
	render() {
		return (
			<div>
				<SpinnerLayer loading={this.state.isSending} />
				<div className="m-3">
					<img src={logo} />
				</div>
				<form
					onSubmit={(event) => {
						event.preventDefault();
						this.handleSubmit();
					}}
				>
					<div className="form-group">
						<input
							type="email"
							placeholder="Email"
							required
							className="form-control"
							onChange={(event) => {
								this.setState({
									email: event.target.value
								});
							}}
						/>
					</div>
					<div className="form-group">
						<input
							type="password"
							placeholder="Password"
							required
							className="form-control"
							onChange={(event) => {
								this.setState({
									password: event.target.value
								});
							}}
						/>
					</div>
					<div className="text-left">
						{this.state.errorMessage ? <p className="text-danger pb-1">{this.state.errorMessage}</p> : ``}
					</div>
					<div className="form-group">
						<button type="submit" className="btn btn-primary form-control" disabled={this.state.isSending}>
							Login
						</button>
						<button
							type="button"
							className="btn btn-link form-control"
							disabled={this.state.isSending}
							onClick={(event) => {
								event.preventDefault();
								this.props.history.push(`${this.props.match.params.parentPath || ''}register`);
							}}
						>
							Register
						</button>
					</div>
				</form>
			</div>
		);
	}
}
