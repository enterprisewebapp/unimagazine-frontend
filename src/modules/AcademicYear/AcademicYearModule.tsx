import React from 'react';
import { Switch, Route } from 'react-router-dom';
import YearList from './YearList';
import { YearModuleProps } from '.';
import YearDetails from './YearDetails';

export default class AcademicYear extends React.Component<YearModuleProps> {
	render() {
		return (
			<Switch>
				<Route
					path={':parentPath(.*)/years/:year_id'}
					render={(routerProps) => {
						var { year_id } = routerProps.match.params;
						const year = this.props.years.data.find((year) => year.configId == year_id);
						if (!year) return <div />;
						return <YearDetails year={year} {...this.props} {...routerProps} />;
					}}
				/>
				<Route
					path={':parentPath(.*)/years'}
					render={(routerProps) => {
						return <YearList {...this.props} {...routerProps} />;
					}}
				/>
				<Route
					path={':parentPath(.*)'}
					render={(routerProps) => {
						return <YearList {...this.props} {...routerProps} />;
					}}
				/>
			</Switch>
		);
	}
}
