import React from 'react';
import { YearSingleProps } from '..';
import moment from 'moment';

export default class YearSingleRow extends React.Component<YearSingleProps> {
	setAsCurrentYear() {
		if(this.props.onUpdate) this.props.onUpdate();
		this.props.setCurrentYear(this.props.year.configId);
	}

	render() {
		return (
			<div>
				<div className="col-12 row table-row-single">
					<div className="col-8">
						<div className="col-12">
							<b className="text-primary">
								{this.props.year.academicYear}{' '}
								{this.props.year.currentFlag ? (
									<span className="text-info">
										<i className="fa fa-check" aria-hidden="true" />
									</span>
								) : null}
							</b>
						</div>
						<div className="col-12">
							<small className="pr-3">
								<b>Contribution closure date</b>:
								{moment(this.props.year.closureDate1).format('YYYY-MM-DD') || '--'}
							</small>
							<small className="pr-3">
								<b>Final closure date</b>:
								{moment(this.props.year.closureDate2).format('YYYY-MM-DD') || '--'}
							</small>
						</div>
					</div>
					<div className="col-4 pt-1">
						<div className="float-right">
							{this.props.year.currentFlag ? null : (
								<button
									className="btn btn-outline-info"
									onClick={(event) => {
										event.preventDefault();
										this.setAsCurrentYear();
									}}
								>
									Set as Current Year
								</button>
							)}
							<button
								className="btn btn-outline-primary ml-2"
								onClick={(event) => {
									event.preventDefault();
									this.props.history.push(
										`${this.props.match.params.parentPath || ''}/years/${this.props.year.configId}`
									);
								}}
							>
								View
							</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
