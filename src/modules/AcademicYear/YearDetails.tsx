import React from 'react';
import { YearSingleProps } from '.';
import moment from 'moment';
import Year from '../../data/Year';
import ReactDatePicker from 'react-datepicker';
import ErrorMessage from '../../layouts/portal/components/ErrorMessage';
import SpinnerLayer from '../../layouts/portal/components/SpinnerLayer';

class State {
	constructor(
		public isEditing: boolean = false,
		public isUpdating: boolean = false,
		public updatedConfig: Year = null,
		public errorMessages: string[] = []
	) {}
}

export default class YearDetails extends React.Component<YearSingleProps, State> {
	constructor(props) {
		super(props);
		this.state = new State();
	}

	componentWillReceiveProps(props: YearSingleProps) {
		if (this.state.isUpdating && !props.years.isUpdating) {
			this.setState({
				isUpdating: false,
				isEditing: false,
				updatedConfig: null
			});
			this.props.loadYears(0, 1000);
		}
	}

	handleUpdate() {
		var validationErrors = [];

		if (!this.state.updatedConfig.termContent) validationErrors.push(`Terms & Conditions are required`);
		if (!this.state.updatedConfig.closureDate1) validationErrors.push(`Contribution Closure Date is required`);
		if (!this.state.updatedConfig.closureDate2) validationErrors.push(`Final Closure Date are required`);

		this.setState({
			errorMessages: validationErrors
		});
		if (validationErrors.length) return;

		this.setState({
			isUpdating: true
		});
		this.props.updateYear(this.state.updatedConfig);
	}

	render() {
		return (
			<div>
				<SpinnerLayer loading={this.state.isUpdating} />
				<div className="d-flex justify-content-between">
					<h2>{this.props.year.academicYear}</h2>
					<div className="justify-content-end">
						{this.state.isEditing ? (
							<div>
								<button
									className="btn btn-secondary"
									onClick={() =>
										this.setState({
											isEditing: false,
											updatedConfig: null
										})}
								>
									Cancel
								</button>
								<button
									className="btn btn-primary ml-2"
									onClick={(event) => {
										event.preventDefault();
										this.handleUpdate();
									}}
								>
									Save
								</button>
							</div>
						) : (
							<button
								className="btn btn-primary"
								onClick={() =>
									this.setState({
										isEditing: true,
										updatedConfig: this.props.year
									})}
							>
								Edit
							</button>
						)}
					</div>
				</div>
				<ErrorMessage messages={this.state.errorMessages} />
				<div className="card mt-2">
					<div className="card-header">Terms & Conditions</div>
					<div className="card-body">
						{this.state.isEditing ? (
							<textarea
								required
								className="form-control"
								value={this.state.updatedConfig.termContent}
								onChange={(event) =>
									this.setState({
										updatedConfig: Object.assign({}, this.state.updatedConfig, {
											termContent: event.target.value
										})
									})}
							/>
						) : (
							<p>{this.props.year.termContent}</p>
						)}
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Contribution Closure Date</div>
					<div className="card-body">
						{this.state.isEditing ? (
							<ReactDatePicker
								required
								minDate={new Date()}
								className="form-control"
								value={moment(this.state.updatedConfig.closureDate1).format('YYYY-MM-DD')}
								onChange={(date) =>
									this.setState({
										updatedConfig: Object.assign({}, this.state.updatedConfig, {
											closureDate1: date
										})
									})}
							/>
						) : (
							<p>{moment(this.props.year.closureDate1 || '').format('YYYY-MM-DD')}</p>
						)}
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Final Closure Date</div>
					<div className="card-body">
						{this.state.isEditing ? (
							<ReactDatePicker
								required
								minDate={this.state.updatedConfig.closureDate1}
								className="form-control"
								value={moment(this.state.updatedConfig.closureDate2).format('YYYY-MM-DD')}
								onChange={(date) =>
									this.setState({
										updatedConfig: Object.assign({}, this.state.updatedConfig, {
											closureDate2: date
										})
									})}
							/>
						) : (
							<p>{moment(this.props.year.closureDate2 || '').format('YYYY-MM-DD')}</p>
						)}
					</div>
				</div>
				<div>
					<button className="btn btn-link p-0 mt-2" onClick={() => this.props.history.goBack()}>
						Back
					</button>
				</div>
			</div>
		);
	}
}
