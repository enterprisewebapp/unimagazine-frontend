import React from 'react';
import uuid from 'uuid/v4';
import ErrorMessage from '../../layouts/portal/components/ErrorMessage';
import SpinnerLayer from '../../layouts/portal/components/SpinnerLayer';
import YearSingleRow from './components/YearSingleRow';
import Year from '../../data/Year';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { YearModuleProps } from '.';
import _ from 'lodash';

class State {
	constructor(
		public isLoading: boolean = false,
		public isUpdating: boolean = false,
		public isCreatingYear: boolean = false,
		public errorMessage: string = '',
		public years: any[] = [],
		public isAddingYear: boolean = false,
		public newYear: Year = null,
		public pageIndex: number = 0,
		public pageSize: number = 1000
	) {}
}

export default class YearList extends React.Component<YearModuleProps, State> {
	constructor(props) {
		super(props);
		this.state = new State();
	}

	componentDidMount() {
		this.loadYears();
	}

	componentWillReceiveProps(props: YearModuleProps) {
		if (this.state.isUpdating && !props.years.isUpdating) {
			this.setState({
				isUpdating: false
			});
			this.componentDidMount();
		}
		if (this.state.isLoading && !props.years.isLoading) {
			this.setState({
				isLoading: false,
				years: _.orderBy(props.years.data, 'academicYear', 'desc')
			});
			if (props.years.error)
				this.setState({
					errorMessage: props.years.error.message || 'Something went wrong. Please try again later!'
				});
		}
		if (this.state.isCreatingYear && !props.years.isCreating) {
			this.setState({
				isCreatingYear: false
			});
			if (props.years.error) {
				this.setState({
					errorMessage: props.years.error.message || 'Something went wrong. Please try again later!'
				});
			}
			else {
				this.componentDidMount();
			}
		}
	}

	loadYears() {
		this.setState({
			isLoading: true
		});

		this.props.loadYears(this.state.pageIndex, this.state.pageSize);
	}

	handleAddYear() {
		const year = Object.assign({}, this.state.newYear);
		this.setState({
			isAddingYear: false,
			isCreatingYear: true,
			newYear: null
		});
		this.props.createYear(year);
	}

	render() {
		return (
			<div>
				<SpinnerLayer loading={this.state.isLoading || this.state.isUpdating || this.state.isCreatingYear} />
				<div>
					<div className="justify-content-between">
						<h2>Academic years</h2>
					</div>
					{this.state.errorMessage ? <ErrorMessage messages={[ this.state.errorMessage ]} /> : null}
					<div className="list-container">
						{this.state.years.map((year) => {
							return (
								<div className="col">
									<YearSingleRow
										key={uuid()}
										{...this.props}
										year={year}
										onUpdate={() => {
											this.setState({
												isUpdating: true
											});
										}}
									/>
								</div>
							);
						})}
					</div>
					<div className="row col">
						{this.state.isAddingYear ? (
							<div className="card col-12">
								<div className="card-body">
									<form
										onSubmit={(event) => {
											event.preventDefault();
											this.handleAddYear();
										}}
									>
										<div className="form-group">
											<label>Year</label>
											<input
												required
												type="number"
												className="form-control"
												max={new Date().getFullYear() + 1}
												value={(this.state.newYear || ({} as any)).academicYear}
												onChange={(event) => {
													event.preventDefault();
													this.setState({
														newYear: Object.assign({}, this.state.newYear, {
															academicYear: event.target.value
														})
													});
												}}
											/>
										</div>
										<div className="form-group">
											<label>Terms & Conditions</label>
											<textarea
												required
												className="form-control"
												value={(this.state.newYear || ({} as any)).termContent}
												onChange={(event) => {
													event.preventDefault();
													this.setState({
														newYear: Object.assign({}, this.state.newYear, {
															termContent: event.target.value
														})
													});
												}}
											/>
										</div>
										<div className="form-group">
											<label>Contribution Closure Date</label>
											<DatePicker
												required
												className="form-control"
												value={
													moment(
														(this.state.newYear || ({} as any)).closureDate1 || ''
													).isValid() ? (
														moment(
															(this.state.newYear || ({} as any)).closureDate1 || ''
														).format('YYYY-MM-DD')
													) : (
														''
													)
												}
												onChange={(date) => {
													this.setState({
														newYear: Object.assign({}, this.state.newYear, {
															closureDate1: date
														})
													});
												}}
											/>
										</div>
										<div className="form-group">
											<label>Final Closure Date</label>
											<DatePicker
												required
												className="form-control"
												value={
													moment(
														(this.state.newYear || ({} as any)).closureDate2 || ''
													).isValid() ? (
														moment(
															(this.state.newYear || ({} as any)).closureDate2 || ''
														).format('YYYY-MM-DD')
													) : (
														''
													)
												}
												onChange={(date) => {
													this.setState({
														newYear: Object.assign({}, this.state.newYear, {
															closureDate2: date
														})
													});
												}}
											/>
										</div>

										<div className="row col form-group">
											<button
												type="button"
												className="btn btn-secondary mr-2"
												onClick={(event) => {
													event.preventDefault();
													this.setState({
														isAddingYear: false,
														newYear: null
													});
												}}
											>
												Cancel
											</button>
											<button type="submit" className="btn btn-primary">
												Save
											</button>
										</div>
									</form>
								</div>
							</div>
						) : (
							<button
								className="btn btn-primary mt-2"
								onClick={() => {
									this.setState({
										isAddingYear: true
									});
								}}
							>
								Add Year
							</button>
						)}
					</div>
				</div>
			</div>
		);
	}
}
