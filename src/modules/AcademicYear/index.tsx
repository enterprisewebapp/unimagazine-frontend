import { connect } from 'react-redux';
import AcademicYearModule from './AcademicYearModule';
import YearActions from '../../data/Year/actions';
import Year, { YearReducerProps } from '../../data/Year';
import { RouteComponentProps } from 'react-router';

const mapStateToProps = (state) => {
	return state;
};

const mapDispatchToProps = (dispatch) => {
	return {
		createYear: (yearConfig: Year) => {
			dispatch(YearActions.creatYear(yearConfig));
		},
		loadYears: (pageIndex: number, pageSize: number) => {
			dispatch(YearActions.loadYears(pageIndex, pageSize));
		},
		updateYear: (yearConfig: Year) => {
			dispatch(YearActions.updateYear(yearConfig));
		},
		setCurrentYear: (yearId: number) => {
			dispatch(YearActions.setCurrentYear(yearId));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(AcademicYearModule);

export interface YearModuleProps extends RouteComponentProps<{ parentPath: string }> {
	years: YearReducerProps;
	createYear: (yearConfig: Year) => void;
	loadYears: (pageIndex: number, pageSize: number) => void;
	updateYear: (yearConfig: Year) => void;
	setCurrentYear: (yearId: number) => void;
}

export interface YearSingleProps extends YearModuleProps {
	year: Year;
	onUpdate?: () => any
}
