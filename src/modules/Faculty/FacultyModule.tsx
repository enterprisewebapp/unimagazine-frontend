import React from 'react';
import { Switch, Route } from 'react-router';
import FacultyDetails from './FacultyDetails';
import { FacultyModuleProps } from '.';

export default class FacultyModule extends React.Component<FacultyModuleProps> {
	render() {
		return (
			<Switch>
				<Route
					path={`:parentPath(.*)/faculties/:faculty`}
					render={(routerProps) => {
						const { faculty } = routerProps.match.params;
						return <FacultyDetails {...this.props} {...routerProps} faculty={faculty} />;
					}}
				/>
				<Route
					path={`:parentPath(.*)`}
					render={(routerProps) => {
						return null;
					}}
				/>
			</Switch>
		);
	}
}
