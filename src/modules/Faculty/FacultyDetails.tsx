import React, { ReactElement } from 'react';
import { FacultyDetailProps } from '.';
import User, { Faculties, UserRoles } from '../../data/User';
import SpinnerLayer from '../../layouts/portal/components/SpinnerLayer';
import SelectSearch from 'react-select-search';
import 'react-select-search/style.css';
import inflection from 'inflection';
import uuid from 'uuid/v4';

class State {
	constructor(
		public marketingCoordinator: User = null,
		public students: User[] = [],
		public guest: User = null,
		public newGuest: User = null,
		public newMarketingCoordinator: User = null,
		public isSending: boolean = false,
		public isLoading: boolean = false,
		public isAssigningGuest: boolean = false,
		public isAssigningMarketingCoordinator: boolean = false
	) {}
}

export default class FacultyDetails extends React.Component<FacultyDetailProps, State> {
	constructor(props: FacultyDetailProps) {
		super(props);
		this.state = new State();
	}

	componentDidMount() {
		this.loadUsers();
		this.loadArticles();
	}

	componentWillReceiveProps(nextProps: FacultyDetailProps) {
		if (this.props.faculty != nextProps.faculty) {
			this.componentDidMount();
		}
		if (this.state.isLoading && !nextProps.users.isLoading) {
			this.setState({
				isLoading: false
			});
			const users = nextProps.users.data.filter((user) => user.faculty == this.props.faculty);
			var marketingCoordinator = users.find((user) => user.role == UserRoles.MARKETING_COORDINATOR) || null;
			var students = users.filter((user) => user.role == UserRoles.STUDENT) || [];
			var guest = users.find((user) => user.role == UserRoles.GUEST) || null;
			this.setState({
				marketingCoordinator,
				students,
				guest
			});
		}

		if (this.state.isSending && !nextProps.users.isUpdating) {
			this.setState({
				isSending: false
			});
			this.loadUsers();
		}
	}

	loadArticles() {
		this.props.loadThreads(0, 1000, this.props.faculty);
	}

	loadUsers() {
		this.setState({
			isLoading: true,
			isAssigningMarketingCoordinator: false,
			isAssigningGuest: false
		});
		this.props.loadUsers(0, 1000);
	}

	handleAssignUser(userId, role) {
		this.setState({
			isSending: true
		});
		this.props.updateFaculty(userId, this.props.faculty);
		this.props.updateRole(userId, role);
	}

	revokeUser(userId: number) {
		return (event) => {
			event.preventDefault();
			this.setState({
				isSending: true,
				isAssigningGuest: false,
				isAssigningMarketingCoordinator: false
			});
			this.props.updateRole(userId, UserRoles.STUDENT);
		};
	}

	render() {
		var renderUserDetails = (user, options: { key?: string; revokable?: boolean } = { revokable: true }) => {
			return (
				<div className="row" key={options.key}>
					<div className="col-9">
						<div className="col-12">
							<b className="text-primary">{user.fullname}</b>
						</div>
						<div className="col-12">
							<small className="pr-3">
								<b>Email</b>: {user.email || '--'}
							</small>
							<small className="pr-3">
								<b>Address</b>: {user.address || '--'}
							</small>
							<small className="pr-3">
								<b>Role</b>: {inflection.titleize(inflection.humanize(user.role || '')) || '--'}
							</small>
							<small className="pr-3">
								<b>Faculty</b>: {Faculties[user.faculty] || '--'}
							</small>
						</div>
					</div>
					{options.revokable ? (
						<div className="col-3 pt-1">
							<div className="float-right">
								<button
									className="btn btn-outline-danger"
									onClick={this.revokeUser(user.userId).bind(this)}
								>
									Revoke
								</button>
							</div>
						</div>
					) : null}
				</div>
			);
		};
		return (
			<div>
				<SpinnerLayer loading={this.state.isLoading || this.state.isSending} />
				<div className="justify-content-between">
					<h2>{Faculties[this.props.faculty]}</h2>
				</div>
				<div className="card mt-2">
					<div className="card-header">Marketing Coordinator</div>
					<div className="card-body">
						{this.state.marketingCoordinator ? (
							renderUserDetails(this.state.marketingCoordinator)
						) : this.state.isAssigningMarketingCoordinator ? (
							<div>
								<SelectSearch
									options={this.props.users.data
										.filter((user) => user.role != UserRoles.ADMIN)
										.map((user) => {
											return {
												value: user.userId,
												name: user.fullname
											};
										})}
									value={(this.state.newMarketingCoordinator || ({} as any)).userId}
									placeholder="Choose a user"
									onChange={(value) => {
										this.setState({
											newMarketingCoordinator:
												this.props.users.data.find((user) => user.userId == value.value) || null
										});
									}}
									search={true}
								/>
								<button
									type="button"
									className="btn btn-secondary mr-2"
									onClick={(event) => {
										event.preventDefault();
										this.setState({
											isAssigningMarketingCoordinator: false,
											newMarketingCoordinator: null
										});
									}}
								>
									Cancel
								</button>
								<button
									type="button"
									className="btn btn-primary"
									onClick={(event) => {
										event.preventDefault();
										if (!this.state.newMarketingCoordinator) return;
										this.handleAssignUser(
											this.state.newMarketingCoordinator.userId,
											UserRoles.MARKETING_COORDINATOR
										);
									}}
								>
									Save
								</button>
							</div>
						) : (
							<button
								type="button"
								className="btn btn-primary"
								onClick={(event) => {
									event.preventDefault();
									this.setState({
										isAssigningMarketingCoordinator: true
									});
								}}
							>
								Assign user
							</button>
						)}
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Guest</div>
					<div className="card-body">
						{this.state.guest ? (
							renderUserDetails(this.state.guest)
						) : this.state.isAssigningGuest ? (
							<div>
								<SelectSearch
									options={this.props.users.data
										.filter((user) => user.role != UserRoles.ADMIN)
										.map((user) => {
											return {
												value: user.userId,
												name: user.fullname
											};
										})}
									value={(this.state.newGuest || ({} as any)).userId}
									placeholder="Choose a user"
									onChange={(value) => {
										this.setState({
											newGuest:
												this.props.users.data.find((user) => user.userId == value.value) || null
										});
									}}
									search={true}
								/>
								<button
									type="button"
									className="btn btn-secondary mr-2"
									onClick={(event) => {
										event.preventDefault();
										this.setState({
											isAssigningGuest: false,
											newGuest: null
										});
									}}
								>
									Cancel
								</button>
								<button
									type="button"
									className="btn btn-primary"
									onClick={(event) => {
										event.preventDefault();
										if (!this.state.newGuest) return;
										this.handleAssignUser(this.state.newGuest.userId, UserRoles.GUEST);
									}}
								>
									Save
								</button>
							</div>
						) : (
							<button
								type="button"
								className="btn btn-primary"
								onClick={(event) => {
									event.preventDefault();
									this.setState({
										isAssigningGuest: true
									});
								}}
							>
								Assign user
							</button>
						)}
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Statistics</div>
					<div className="card-body">
						<div className="row col-12">
							<div className="col-3">
								<p>
									Number of Contributions: <b>{this.props.articles.data.length}</b>
								</p>
							</div>
							<div className="col-3">
								<p>
									Number of Published Articles:{' '}
									<b>{this.props.articles.data.filter((article) => article.status == 3).length}</b>
								</p>
							</div>
							<div className="col-3">
								<p>
									Pending Articles:{' '}
									<b>{this.props.articles.data.filter((article) => article.status == 0).length}</b>
								</p>
							</div>
							<div className="col-3">
								<p>
									Approved Articles:{' '}
									<b>{this.props.articles.data.filter((article) => article.status == 2).length}</b>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div className="card mt-2">
					<div className="card-header">Students</div>
					<div className="card-body">
						{this.state.students.map((student) => {
							return renderUserDetails(student, {
								key: uuid(),
								revokable: false
							});
						})}
					</div>
				</div>
			</div>
		);
	}
}
