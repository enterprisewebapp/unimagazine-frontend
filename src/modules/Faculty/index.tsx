import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import FacultyModule from './FacultyModule';
import UserActions from '../../data/User/actions';
import { UserReducerProps } from '../../data/User';
import SessionReducerProps from '../../data/Session';
import ThreadActions from '../../data/Article/actions';
import { ArticleReducerProps } from '../../data/Article';

export interface FacultyModuleProps extends RouteComponentProps<{ parentPath: string }> {
	users: UserReducerProps;
	session: SessionReducerProps;
	articles: ArticleReducerProps;
	getByRole: (role: string, pageIndex: number, pageSize: number) => void;
	getByFaculty: (faculty: string, pageIndex: number, pageSize: number) => void;
	loadUsers: (pageIndex: number, pageSize: number) => void;
	updateFaculty: (userId: number, faculty: string) => void;
	updateRole: (userId: number, role: string) => void;
	loadThreads: (pageIndex: number, pageSize: number, faculty: string) => void;
}

export interface FacultyDetailProps extends FacultyModuleProps {
	faculty: string;
}

const mapStateToProps = (state) => {
	return state;
};

const mapDispatchToProps = (dispatch) => {
	return {
		getByRole: (role: string, pageIndex: number, pageSize: number) => {
			dispatch(UserActions.getByRole(role, pageIndex, pageSize));
		},
		getByFaculty: (faculty: string, pageIndex: number, pageSize: number) => {
			dispatch(UserActions.getByFaculty(faculty, pageIndex, pageSize));
		},
		loadUsers: (pageIndex: number, pageSize: number) => {
			dispatch(UserActions.loadUsers(pageIndex, pageSize));
		},
		updateFaculty: (userId: number, faculty: string) => {
			dispatch(UserActions.setFaculty(userId, faculty));
		},
		updateRole: (userId: number, role: string) => {
			dispatch(UserActions.setRole(userId, role));
		},
		loadThreads: (pageIndex: number, pageSize: number, faculty: string) => {
			dispatch(ThreadActions.load(pageIndex, pageSize, faculty));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(FacultyModule);
