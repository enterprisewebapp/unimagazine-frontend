import * as React from 'react';
import PortalLayout from './layouts/portal';
import './assets/css/site.css';
import { connect } from 'react-redux';
import SessionReducerProps from './data/Session';
import AuthModule from './modules/Auth';
import { BrowserRouter, RouteComponentProps, Switch, Route, HashRouter } from 'react-router-dom';
import SessionActions from './data/Session/actions';
import ArticleModule from './modules/Article';
import UserModule from './modules/User';
import FacultyModule from './modules/Faculty';
import AcademicYearModule from './modules/AcademicYear';
import 'react-datepicker/dist/react-datepicker.css';
import './assets/images/icon.png';
import { UserRoles } from './data/User';

export interface AppProps extends RouteComponentProps<{ parentPath: string }> {
	session: SessionReducerProps;
	logout: () => void;
}

class App extends React.Component<AppProps> {
	componentWillReceiveProps(nextProps: AppProps) {
		this.authSession(nextProps);
	}
	authSession(props: AppProps) {
		Object.keys(props).forEach((key) => {
			var reducer = props[key];
			var error = reducer.error || {};
			error.response = error.response || {};
			if (error.response.status === 401) {
				props.logout();
			}
		});
	}
	render() {
		return (
			<HashRouter>
				{this.props.session.token ? (
					<PortalLayout {...this.props as any}>
						<div className="container" style={{ marginTop: '80px' }}>
							{this.props.session.userProfile.faculty ||
							this.props.session.userProfile.role == UserRoles.ADMIN ||
							this.props.session.userProfile.role == UserRoles.MARKETING_MANAGER ? (
								<Switch>
									<Route
										path="/articles"
										render={(routerProps) => {
											return <ArticleModule {...this.props as any} {...routerProps} />;
										}}
									/>
									<Route
										path="/users"
										render={(routerProps) => {
											return <UserModule {...this.props as any} {...routerProps} />;
										}}
									/>
									<Route
										path="/faculties"
										render={(routerProps) => {
											return <FacultyModule {...this.props as any} {...routerProps} />;
										}}
									/>
									<Route
										path="/years"
										render={(routerProps) => {
											return <AcademicYearModule {...this.props as any} {...routerProps} />;
										}}
									/>
									<Route
										path="/"
										render={(routerProps) => {
											return <ArticleModule {...this.props as any} {...routerProps} />;
										}}
									/>
								</Switch>
							) : (
								<div>
									<h4 className="text-danger">
										Oops! You are not assigned to any faculty. Please contact your adminstrator for
										more info
									</h4>
								</div>
							)}
						</div>
					</PortalLayout>
				) : (
					<Switch>
						<Route
							path={'*'}
							render={(routerProps) => {
								return <AuthModule {...this.props} {...routerProps} />;
							}}
						/>
					</Switch>
				)}
			</HashRouter>
		);
	}
}

const mapStateToProps = (state: any) => {
	return state;
};

const mapDispatchToProps = (dispatch: any) => {
	return {
		logout: () => {
			dispatch(SessionActions.logout());
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
