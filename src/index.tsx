import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore, applyMiddleware, AnyAction } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './data/rootReducer';
import thunk from 'redux-thunk';
import App from './App';
import SessionActions, { Types } from './data/Session/actions';
import API from './api';

let persistedReduxState: { [key: string]: any | null } = {};
const storedState = localStorage.getItem(`ewsd`);

if (storedState) {
	persistedReduxState = JSON.parse(storedState);
}

const store = createStore(rootReducer, persistedReduxState, composeWithDevTools(applyMiddleware(thunk)));

store.subscribe(() => {
	let state = store.getState();
	let forbidden = false;
	Object.keys(state).forEach((key) => {
		var reducer = state[key];
		if (reducer.error && reducer.error.name == 'TokenExpiredError') {
			forbidden = true;
		}
	});
	if (forbidden && state.session.token) {
		store.dispatch(SessionActions.logout() as any);
	}
	localStorage.setItem('ewsd', JSON.stringify(state));
	if (state.session.token) {
		API.shared.api.defaults.headers.Authorization = state.session.token;
	} else {
		delete API.shared.api.defaults.headers['Authorization'];
	}
});

ReactDOM.render(
	<Provider store={store}>
		<App {...store.getState() as any} />
	</Provider>,
	document.getElementById('main')
);
