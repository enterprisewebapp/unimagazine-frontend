import React from "react"
import PortalNavBar from "./components/NavBar";
import { AppProps } from "../../App";

export interface PortalLayoutProps extends AppProps {
    
}

class PortalLayout extends React.Component<PortalLayoutProps> {

    render() {
        return (
            <main>
                <PortalNavBar {...this.props} />
                {this.props.children}
            </main>
        )
    }
}

export default PortalLayout