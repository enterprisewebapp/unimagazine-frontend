import React from "react";
import {HashLoader} from "react-spinners"

export default class SpinnerLayer extends React.Component<{
    loading: boolean
}> {
    render() {
        return (
            <div className={`spinner-layer ${!this.props.loading ? "d-none" : ""}`}>
                <HashLoader loading={this.props.loading} size={50} color={`#007bff`} />
            </div>
        )
    }
}