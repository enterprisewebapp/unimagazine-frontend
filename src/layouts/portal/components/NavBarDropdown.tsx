import React from 'react'

export default class NavBarDropdown extends React.Component<{title: string}> {
    render() {
        return (
            <div className="dropdown navbar-nav">
                <a className="nav-item nav-link dropdown-toggle" href="/" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.props.title}</a>
                <div className="dropdown-menu dropdown-menu-right">
                    {this.props.children}
                </div>
            </div>
        )
    }
}