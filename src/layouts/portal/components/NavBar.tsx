import React from 'react';
import { UserRoles, Faculties } from '../../../data/User';
import { PortalLayoutProps } from '..';
import { NavLink } from 'react-router-dom';
import NavBarDropdown from './NavBarDropdown';
import uuid from 'uuid/v4';

export default class PortalNavBar extends React.Component<PortalLayoutProps> {
	componentDidMount() {
		this.setup();
	}
	setup() {
		var $navItems = document.getElementsByClassName('nav-item');
		var navItems = Array.from($navItems);
		navItems.forEach((item) => {
			item.addEventListener('click', () => {
				navItems.forEach((i) => {
					i.classList.remove('active');
				});
				item.classList.add('active');
			});
		});
	}

	render() {
		const renderMenuLinks = (role: UserRoles) => {
			const isAdmin = role == UserRoles.ADMIN;
			const isMarketingManager = role == UserRoles.MARKETING_MANAGER;
			const isMarketingCoordinator = role == UserRoles.MARKETING_COORDINATOR;
			const isStudent = role == UserRoles.STUDENT;
			const isGuest = role == UserRoles.GUEST;
			return (
				<ul className="navbar-nav mr-auto mt-2 mt-lg-0">
					<li className="nav-item">
						<NavLink className="nav-link" to={`/articles`}>
							Articles
						</NavLink>
					</li>
					{isAdmin ? (
						<li className="nav-item">
							<NavLink className="nav-link" to={`/users`}>
								Users
							</NavLink>
						</li>
					) : null}
					{isAdmin ? (
						<li className="nav-item">
							<NavBarDropdown title="Faculties">
								{Object.keys(Faculties).map((faculty) => {
									return (
										<NavLink key={uuid()} className="dropdown-item" to={`/faculties/${faculty}`}>
											{Faculties[faculty]}
										</NavLink>
									);
								})}
							</NavBarDropdown>
						</li>
					) : null}
					{isAdmin ? <li>
						<NavLink className="nav-link" to="/years">
							Academic years
						</NavLink>
					</li> : null}
				</ul>
			);
		};
		return (
			<nav className="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
				<div className="container">
					<a className="navbar-brand" href="./">
						EWSD Magazine
					</a>
					<button
						className="navbar-toggler"
						type="button"
						data-toggle="collapse"
						data-target="#navbarToggler"
						aria-controls="navbarToggler"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon" />
					</button>
					<div className="collapse navbar-collapse" id="navbarToggler">
						{renderMenuLinks(
							this.props.session.userProfile ? this.props.session.userProfile.role : UserRoles.GUEST
						)}
						<div className="dropdown">
							<button
								style={{ color: 'white' }}
								className="btn btn-link nav-link nav-item dropdown-toggle"
								id="dropdownNavButton"
								data-toggle="dropdown"
								aria-haspopup="true"
								aria-expanded="false"
							>
								{this.props.session.userProfile ? this.props.session.userProfile.fullname : 'Guest'}
							</button>
							<div className="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownNavButton">
								<button
									className="btn btn-link dropdown-item"
									onClick={() => {
										this.props.logout();
									}}
								>
									Logout
								</button>
							</div>
						</div>
					</div>
				</div>
			</nav>
		);
	}
}
