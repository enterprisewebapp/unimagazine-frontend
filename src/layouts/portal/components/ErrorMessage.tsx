import React from 'react';
import uuid from 'uuid/v4';

export default class ErrorMessage extends React.Component<{ messages: string[] }> {
	render() {
		return this.props.messages.map((message) => {
			return (
				<p key={uuid()} className="text-danger">
					{message}
				</p>
			);
		});
	}
}
