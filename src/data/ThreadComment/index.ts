import User from '../User';

export default class ThreadComment {
	commentId: number;
	content: string;
	createdBy: User;
	createdDate: Date;
	updatedDate: Date;
}
