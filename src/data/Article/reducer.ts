import { ArticleReducerProps } from '.';
import { ReduxAction } from '..';
import { Types } from '../Article/actions';

const initialState: ArticleReducerProps = {
	data: [],
	pagination: {
		currentPage: 0,
		itemsPerPage: 1000,
		totalCount: 0
	},
	isLoading: false,
	isCreating: false,
	isUpdating: false,
	isRejecting: false,
	isApproving: false,
	isDeleting: false,
	isCommentCreating: false,
	error: null
};

export default (state: ArticleReducerProps = initialState, action: ReduxAction) => {
	switch (action.type) {
		case Types.ARTICLE_WILL_UPDATE:
			return Object.assign({}, state, {
				isUpdating: true
			});
		case Types.ARTICLE_DID_UPDATE:
			return Object.assign({}, state, {
				isUpdating: false,
				error: action.error || null
			});
		case Types.COMMENT_WILL_CREATE:
			return Object.assign({}, state, {
				isCommentCreating: true
			});
		case Types.COMMENT_DID_CREATE:
			return Object.assign({}, state, {
				isCommentCreating: false,
				error: action.error || null
			});
		case Types.ARTICLE_WILL_LOAD:
			return Object.assign({}, state, {
				isLoading: true
			});
		case Types.ARTICLE_DID_LOAD:
			return Object.assign({}, state, {
				isLoading: false,
				data: action.data ? action.data.list || [] : [],
				error: action.error || null,
				pagination: action.data ? action.data.pagination || initialState.pagination : initialState.pagination
			});
		case Types.ARTICLE_WILL_CREATE:
			return Object.assign({}, state, {
				isCreating: true
			});
		case Types.ARTICLE_DID_CREATE:
			return Object.assign({}, state, {
				isCreating: false,
				error: action.error || null
			});
		default:
			return state;
	}
};
