import { Dispatch } from 'redux';
import { ReduxAction } from '..';
import { ArticleFormData } from '.';
import API from '../../api';
const REQUEST = API.shared;

export const Types = {
	ARTICLE_WILL_CREATE: 'ARTICLE_WILL_CREATE',
	ARTICLE_DID_CREATE: 'ARTICLE_DID_CREATE',
	ARTICLE_WILL_LOAD: 'ARTICLE_WILL_LOAD',
	ARTICLE_DID_LOAD: 'ARTICLE_DID_LOAD',
	COMMENT_WILL_CREATE: 'COMMENT_WILL_CREATE',
	COMMENT_DID_CREATE: 'COMMENT_DID_CREATE',
	ARTICLE_WILL_UPDATE: 'ARTICLE_WILL_UPDATE',
	ARTICLE_DID_UPDATE: 'ARTICLE_DID_UPDATE'
};

export default class ArticleActions {
	static updateStatus(status: number, threadId) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.ARTICLE_WILL_UPDATE
			});

			REQUEST.post('thread/update/status', {
				threadId,
				threadStatus: status
			})
				.then((response) => {
					dispatch({
						type: Types.ARTICLE_DID_UPDATE,
						error: response.data.message == 'updated' ? null : response.data
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.ARTICLE_DID_UPDATE,
						error: error.response.data
					});
				});
		};
	}

	static addComment(comment: string, threadId: number) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.COMMENT_WILL_CREATE
			});

			REQUEST.post('thread/comment', {
				content: comment,
				threadId
			})
				.then((response) => {
					dispatch({
						type: Types.COMMENT_DID_CREATE,
						error: response.data.message == 'saved' ? null : response.data
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.COMMENT_DID_CREATE,
						error: error.response.data
					});
				});
		};
	}

	static load(pageIndex: number, pageSize: number, faculty?: string) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.ARTICLE_WILL_LOAD
			});
			REQUEST.get(`thread/list${faculty ? `/faculty` : ''}`, {
				pageIndex,
				pageSize,
				faculty
			})
				.then((response) => {
					dispatch({
						type: Types.ARTICLE_DID_LOAD,
						data: {
							list: response.data.threadLst,
							pagination: {
								totalCount: response.data.total,
								currentPage: pageIndex,
								itemsPerPage: pageSize
							}
						}
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.ARTICLE_DID_LOAD,
						error: error.response.data
					});
				});
		};
	}

	static update(data: any) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.ARTICLE_WILL_UPDATE
			});

			var formData = new FormData();
			formData.append('threadId', data.threadId);
			formData.append('wordFile', data.wordFile);
			Array.from(data.images).forEach((image: File) => {
				formData.append('images', image, image.name);
			});
			REQUEST.post('thread/update', formData)
				.then((response) => {
					dispatch({
						type: Types.ARTICLE_DID_UPDATE,
						error: response.data.message == 'updated' ? null : response.data
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.ARTICLE_DID_UPDATE,
						error: error.response.data
					});
				});
		};
	}

	static create(data: ArticleFormData) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.ARTICLE_WILL_CREATE
			});

			var formData = new FormData();
			formData.append('threadTitle', data.threadTitle);
			formData.append('description', data.description);
			formData.append('wordFile', data.wordFile);
			Array.from(data.images).forEach((image) => {
				formData.append('images', image, image.name);
			});
			REQUEST.post('thread/create', formData)
				.then((response) => {
					dispatch({
						type: Types.ARTICLE_DID_CREATE,
						error: response.data.message == 'saved' ? null : response.data
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.ARTICLE_DID_CREATE,
						error: error.response.data
					});
				});
		};
	}
}
