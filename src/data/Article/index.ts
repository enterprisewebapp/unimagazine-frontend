import User from '../User';
import ThreadComment from '../ThreadComment';
import ThreadFile from '../ThreadFile';

export default class Article {
	public threadId: number;
	public threadTitle: string;
	public description: string;
	public createdBy: User;
	public faculty: string;
	public takenBy?: User;
	public status: number;
	public threadComments: ThreadComment[];
	public threadFiles: ThreadFile[];
	public createdDate?: Date;
	public updatedDate?: Date;
}

export interface ArticleReducerProps {
	data: Article[];
	pagination: {
		totalCount: number;
		currentPage: number;
		itemsPerPage: number;
	};
	isLoading: boolean;
	isCreating: boolean;
	isUpdating: boolean;
	isDeleting: boolean;
	isApproving: boolean;
	isRejecting: boolean;
	isCommentCreating: boolean;
	error: any;
}

export class ArticleFormData {
	public threadTitle: string;
	public description: string;
	public wordFile: File;
	public images: File[];
}

export const ArticleStatus = {
	0: 'Pending',
	1: 'Rejected',
	2: 'Approve',
	3: 'Published'
};
