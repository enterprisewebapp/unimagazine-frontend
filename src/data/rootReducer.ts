import { combineReducers } from 'redux';
import sessionReducer from './Session/reducer';
import userReducer from './User/reducer';
import yearReducer from './Year/reducer';
import articleReducer from './Article/reducer';

const appReducer = combineReducers({
	session: sessionReducer,
	users: userReducer,
	years: yearReducer,
	articles: articleReducer
});

const rootReducer = (state, action) => {
	if (action.type == 'USER_DID_LOG_OUT') {
		state = undefined;
	}
	return appReducer(state, action);
};

export default rootReducer;
