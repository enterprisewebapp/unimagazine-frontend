export default class Year {
	public configId: number;
	public academicYear: number;
	public termContent: string;
	public closureDate1?: Date;
	public closureDate2?: Date;
	public currentFlag?: number;
	public createdDate?: Date;
	public updatedDate?: Date;
}

export interface YearReducerProps {
	data: Year[];
	pagination: {
		totalCount: number;
		currentPage: number;
		itemsPerPage: number;
	};
	error: any;
	isLoading: boolean;
	isUpdating: boolean;
	isDeleting: boolean;
	isCreating: boolean;
}
