import { Dispatch } from 'redux';
import { ReduxAction } from '..';
import API from '../../api';
import Year from '.';
export const Types = {
	YEAR_WILL_LOAD: 'YEAR_WILL_LOAD',
	YEAR_DID_LOAD: 'YEAR_DID_LOAD',
	YEAR_WILL_UPDATE: 'YEAR_WILL_UPDATE',
	YEAR_DID_UPDATE: 'YEAR_DID_UPDATE',
	YEAR_WILL_DELETE: 'YEAR_WILL_DELETE',
	YEAR_DID_DELETE: 'YEAR_DID_DELETE',
	YEAR_WILL_CREATE: 'YEAR_WILL_CREATE',
	YEAR_DID_CREATE: 'YEAR_DID_CREATE'
};
const REQUEST = API.shared;

export default class YearActions {
	static setCurrentYear(yearId: number) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.YEAR_WILL_UPDATE
			})

			REQUEST.post('year-config/update/status', {configId: yearId})
			.then(response => {
				dispatch({
					type: Types.YEAR_DID_UPDATE,
					error: response.data.message != 'updated_crnt_yr_cfg' ? response.data : null
				});
			})
			.catch(error => {
				dispatch({
					type: Types.YEAR_DID_UPDATE,
					error: error.response.data
				});
			})
		}
	}

	static updateYear(yearConfig: Year) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.YEAR_WILL_UPDATE
			});

			REQUEST.post('year-config/update', yearConfig)
				.then((response) => {
					dispatch({
						type: Types.YEAR_DID_UPDATE,
						error: response.data.message != 'updated_yr_cfg' ? response.data : null
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.YEAR_DID_UPDATE,
						error: error.response.data
					});
				});
		};
	}

	static creatYear(yearConfig: Year) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.YEAR_WILL_CREATE
			});

			REQUEST.post('year-config/create', yearConfig)
				.then((response) => {
					dispatch({
						type: Types.YEAR_DID_CREATE,
						error: response.data.message != 'saved' ? response.data : null
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.YEAR_DID_CREATE,
						error: error.response.data
					});
				});
		};
	}

	static loadYears(pageIndex: number, pageSize: number) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.YEAR_WILL_LOAD
			});

			REQUEST.get('year-config/list', {
				pageIndex,
				pageSize
			})
				.then((response) => {
					dispatch({
						type: Types.YEAR_DID_LOAD,
						data: {
							list: response.data.yrCfgLst,
							pagination: {
								totalCount: response.data.total,
								currentPage: pageIndex,
								itemsPerPage: pageSize
							}
						}
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.YEAR_DID_LOAD,
						error: error.response.data
					});
				});
		};
	}
}
