import { YearReducerProps } from '.';
import { ReduxAction } from '..';
import { Types } from './actions';

const initialState: YearReducerProps = {
	data: [],
	pagination: {
		totalCount: 0,
		currentPage: 0,
		itemsPerPage: 20
	},
	error: null,
	isLoading: false,
	isUpdating: false,
	isDeleting: false,
	isCreating: false
};

export default (state: YearReducerProps = initialState, action: ReduxAction) => {
	switch (action.type) {
		case Types.YEAR_WILL_UPDATE:
			return Object.assign({}, state, {
				isUpdating: true
			});
		case Types.YEAR_DID_UPDATE:
			return Object.assign({}, state, {
				isUpdating: false,
				error: action.error || null
			});
		case Types.YEAR_WILL_CREATE:
			return Object.assign({}, state, {
				isCreating: true
			});
		case Types.YEAR_DID_CREATE:
			return Object.assign({}, state, {
				isCreating: false,
				error: action.error || null
			});
		case Types.YEAR_WILL_LOAD:
			return Object.assign({}, state, {
				isLoading: true
			});
		case Types.YEAR_DID_LOAD:
			return Object.assign({}, state, {
				isLoading: false,
				data: action.data ? action.data.list || [] : [],
				error: action.error || null,
				pagination: action.data ? action.data.pagination || initialState.pagination : initialState.pagination
			});
		default:
			return state;
	}
};
