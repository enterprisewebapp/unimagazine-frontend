export default class User {
	public userId: number;
	public fullname: string;
	public email: string;
	public address?: string;
	public role: UserRoles;
	public faculty: string;
	public status: number;
	public createdDate: Date;
	public updatedDate: Date;
}
export enum UserRoles {
	ADMIN = 'admin',
	MARKETING_MANAGER = 'marketing_manager',
	MARKETING_COORDINATOR = 'marketing_coordinator',
	STUDENT = 'student',
	GUEST = 'guest'
}

export const Faculties: {
	[key: string]: string,
	business: string,
	computer_science: string
} = {
	business: "Business",
	computer_science: "Computer Science"
}

export interface UserReducerProps {
	data: User[];
	pagination: {
		totalCount: number;
		currentPage: number;
		itemsPerPage: number;
	};
	error: any;
	isLoading: boolean;
	isUpdating: boolean;
	isDeleting: boolean;
}
