import { Dispatch } from 'redux';
import { ReduxAction } from '..';
import API from '../../api';
import User from '../User';
const REQUEST = API.shared;
export const Types = {
	USER_WILL_LOAD: 'USER_WILL_LOAD',
	USER_DID_LOAD: 'USER_DID_LOAD',
	USER_WILL_UPDATE: 'USER_WILL_UPDATE',
	USER_DID_UPDATE: 'USER_DID_UPDATE',
	USER_WILL_DELETE: 'USER_WILL_DELETE',
	USER_DID_DELETE: 'USER_DID_DELETE'
};

export default class UserActions {
	static getByFaculty(faculty: string, pageIndex: number, pageSize: number) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_LOAD
			});

			REQUEST.get('user/list/role', {
				faculty,
				pageIndex,
				pageSize
			})
				.then((response) => {
					dispatch({
						type: Types.USER_DID_LOAD,
						data: {
							list: response.data.usrLst,
							pagination: {
								totalCount: response.data.total,
								currentPage: pageIndex,
								itemsPerPage: pageSize
							}
						}
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_LOAD,
						error: error.response.data
					});
				});
		};
	}

	static getByRole(role: string, pageIndex: number, pageSize: number) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_LOAD
			});

			REQUEST.get('user/list/role', {
				role,
				pageIndex,
				pageSize
			})
				.then((response) => {
					dispatch({
						type: Types.USER_DID_LOAD,
						data: {
							list: response.data.usrLst,
							pagination: {
								totalCount: response.data.total,
								currentPage: pageIndex,
								itemsPerPage: pageSize
							}
						}
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_LOAD,
						error: error.response.data
					});
				});
		};
	}

	static setRole(usrId: number, usrRole: string) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_UPDATE
			});

			REQUEST.post('adm/set-role', {
				usrId,
				usrRole
			})
				.then((response) => {
					dispatch({
						type: Types.USER_DID_UPDATE,
						error: response.data.message != 'updated_user_role' ? response.data : null
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_UPDATE,
						error: error.response.data
					});
				});
		};
	}

	static setFaculty(usrId: number, usrFaculty: string) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_UPDATE
			});

			REQUEST.post('adm/set-faculty', {
				usrId,
				usrFaculty
			})
				.then((response) => {
					dispatch({
						type: Types.USER_DID_UPDATE,
						error: response.data.message != 'updated_user_faculty' ? response.data : null
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_UPDATE,
						error: error.response.data
					});
				});
		};
	}

	static update(user: User) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_UPDATE
			});

			REQUEST.post('user/update-info', user)
				.then((response) => {
					dispatch({
						type: Types.USER_DID_UPDATE,
						error: response.data.message != 'updated_user_info' ? response.data : null
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_UPDATE,
						error: error.response.data
					});
				});
		};
	}

	static loadUsers(pageIndex: number, pageSize: number) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_LOAD
			});

			REQUEST.get('user/list', {
				pageIndex,
				pageSize
			})
				.then((response) => {
					dispatch({
						type: Types.USER_DID_LOAD,
						data: {
							list: response.data.usrLst,
							pagination: {
								totalCount: response.data.total,
								currentPage: pageIndex,
								itemsPerPage: pageSize
							}
						}
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_LOAD,
						error: error.response.data
					});
				});
		};
	}
}
