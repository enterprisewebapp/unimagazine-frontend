import { UserReducerProps } from '.';
import { ReduxAction } from '..';
import { Types } from './actions';

const initialState: UserReducerProps = {
	data: [],
	pagination: {
		totalCount: 0,
		currentPage: 0,
		itemsPerPage: 20
	},
	error: null,
	isLoading: false,
	isUpdating: false,
	isDeleting: false
};

export default (state: UserReducerProps = initialState, action: ReduxAction) => {
	switch (action.type) {
		case Types.USER_WILL_UPDATE:
			return Object.assign({}, state, {
				isUpdating: true
			});
		case Types.USER_DID_UPDATE:
			return Object.assign({}, state, {
				isUpdating: false,
				error: action.error || null
			});
		case Types.USER_WILL_LOAD:
			return Object.assign({}, state, {
				isLoading: true
			});
		case Types.USER_DID_LOAD:
			return Object.assign({}, state, {
				isLoading: false,
				data: action.data ? action.data.list || [] : [],
				error: action.error || null,
				pagination: action.data ? action.data.pagination || initialState.pagination : initialState.pagination
			});
		default:
			return state;
	}
};
