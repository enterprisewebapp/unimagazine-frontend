export default class ThreadFile {
	fileId: number;
	path: string;
	fileType: string;
	createdDate: Date;
	updatedDate: Date;
}
