import User from '../User';

export default interface SessionReducerProps {
	token: string;
	userProfile: User;
	error: any;
	isLoggingIn: boolean;
	isRegistering: boolean;
};
