import SessionReducerProps from '.';
import { ReduxAction } from '..';
import { Types } from './actions';
import User from '../User';

const initialState: SessionReducerProps = {
	token: '',
	userProfile: null,
	error: null,
	isLoggingIn: false,
	isRegistering: false
};

export default (state: SessionReducerProps = initialState, action: ReduxAction) => {
	switch (action.type) {
		case Types.USER_DID_REGISTER:
			return Object.assign({}, initialState, {
				isRegistering: false,
				error: action.error ? action.error : null
			});
		case Types.USER_WILL_REGISTER:
			return Object.assign({}, initialState, {
				isRegistering: true
			});
		case Types.USER_DID_LOG_OUT:
			return initialState;
		case Types.USER_WILL_AUTH:
			return Object.assign({}, initialState, {
				isLoggingIn: true
			});
		case Types.USER_DID_AUTH:
			let data: {
				token: string;
				user: User;
			} = action.data || {
				token: '',
				user: null
			};
			return Object.assign({}, state, {
				token: data.token,
				userProfile: data.user,
				error: action.error || null,
				isLoggingIn: false
			});
		default:
			return state;
	}
};
