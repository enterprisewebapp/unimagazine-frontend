import { Dispatch } from 'redux';
import { ReduxAction } from '..';
import API from '../../api';
import User from '../User';
const REQUEST = API.shared;

export const Types = {
	USER_WILL_AUTH: 'USER_WILL_AUTH',
	USER_DID_AUTH: 'USER_DID_AUTH',
	USER_WILL_LOG_OUT: 'USER_WILL_LOG_OUT',
	USER_DID_LOG_OUT: 'USER_DID_LOG_OUT',
	USER_WILL_REGISTER: 'USER_WILL_REGISTER',
	USER_DID_REGISTER: 'USER_DID_REGISTER'
};

export default class SessionActions {
	static register(user: Partial<User>) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_REGISTER
			});

			REQUEST.post('user/register', user)
				.then((response) => {
					dispatch({
						type: Types.USER_DID_REGISTER,
						error:
							response.data.message === 'saved'
								? undefined
								: { message: `Something went wrong. Please try again later!` }
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_REGISTER,
						error: error.response.data
					});
				});
		};
	}

	static login(email: string, password: string) {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_WILL_AUTH
			});

			REQUEST.post(`user/login`, { email, psw: password })
				.then((response) => {
					dispatch({
						type: Types.USER_DID_AUTH,
						data: response.data
					});
				})
				.catch((error) => {
					dispatch({
						type: Types.USER_DID_AUTH,
						error: error.response.data
					});
				});
		};
	}

	static logout() {
		return (dispatch: Dispatch<ReduxAction>) => {
			dispatch({
				type: Types.USER_DID_LOG_OUT
			});
		};
	}
}
