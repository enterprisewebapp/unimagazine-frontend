import { Action } from 'redux';

export interface ReduxAction extends Action<String> {
	data?: any;
	error?: any;
}
