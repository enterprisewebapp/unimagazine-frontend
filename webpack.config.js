const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';
const API_URL = {
	development: 'http://ec2-13-229-85-229.ap-southeast-1.compute.amazonaws.com:3001/api/v1',
	production: 'http://ec2-13-229-85-229.ap-southeast-1.compute.amazonaws.com:3001/api/v1'
}[NODE_ENV];

const FILE_ROOT = {
	development: 'http://ec2-13-229-85-229.ap-southeast-1.compute.amazonaws.com:3001',
	production: 'http://ec2-13-229-85-229.ap-southeast-1.compute.amazonaws.com:3001'
}[NODE_ENV];

module.exports = {
	entry: './src/index.tsx',
	output: {
		filename: 'bundle.js',
		path: __dirname + '/dist'
	},
	devtool: 'source-map',
	resolve: {
		// Add '.ts' and '.tsx' as resolvable extensions.
		extensions: [ '.ts', '.tsx', '.js', 'jsx', '.json' ]
	},
	module: {
		rules: [
			// All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
			{ test: /\.tsx?$/, loader: 'awesome-typescript-loader' },

			// All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
			{ enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
			{
				test: /\.html$/,
				use: [
					{
						loader: 'html-loader'
					}
				]
			},
			{
				test: /\.css$/,
				use: [ 'style-loader', 'css-loader' ]
			},
			{
				test: /\.(jpe?g|png|svg|gif)$/,
				use: {
					loader: 'file-loader?name=./assets/images/[name].[ext]'
				}
			},
			{
				test: /\.mp3$/,
				use: {
					loader: 'file-loader?name=./assets/sounds/[name].[ext]'
				}
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				use: [ 'file-loader' ]
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}
			}
		]
	},

	// When importing a module whose path matches one of the following, just
	// assume a corresponding global variable exists and use that instead.
	// This is important because it allows us to avoid bundling all of our
	// dependencies, which allows browsers to cache those libraries between builds.
	// externals: {
	//     "react": "React",
	//     "react-dom": "ReactDOM"
	// },
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		port: 8000
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: './src/index.html',
			filename: './index.html'
		}),
		new webpack.DefinePlugin({
			NODE_ENV: JSON.stringify(NODE_ENV),
			API_URL: JSON.stringify(API_URL),
			FILE_ROOT: JSON.stringify(FILE_ROOT)
		}),
		new CopyWebpackPlugin([ { from: __dirname + '/src/assets' } ])
	]
};
